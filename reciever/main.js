import './style.css'
import Peer from "peerjs";


window.addEventListener('DOMContentLoaded',()=>{
  const peer = new Peer("test", {
    debug: 3,
    config: {
      iceServers: [{ urls: `stun:stun.l.google.com:19302` }],
    },
    host: "localhost",
    port: 9000,
    path: "/stream",
  });
  const video = document.querySelector('video');
  peer.on("call", (call) => {
    call.answer();
    call.on("stream", (stream) => {
      video.srcObject = stream;
      video.play();
    });
  });
})
