# PoC - PeerJs only transmit audio



## Getting started

Install dependencies in each of the 3 directories 

Start the server with the **node peerserver.js** command 

Run the development server for **reciever/broadcaster** with the command **yarn dev/npm run dev**

Go to localhost:3000 and localhost:3001 after clicking Play and Call on the broadcaster page. There should only be audio with no video.
