const path = require("path");
const ExpressPeerServer = require("peer").ExpressPeerServer;
const express = require("express");
const app = express();
const server = require("http").createServer(app);

const peerServer = ExpressPeerServer(server);
app.use("/stream", peerServer);


server.listen(9000, () => {
  console.log("Server run 🚀 ");
});
